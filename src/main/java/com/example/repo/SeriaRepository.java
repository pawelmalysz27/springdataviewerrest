package com.example.repo;


import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Seria;

public interface SeriaRepository extends JpaRepository<Seria, Long>{
	
	
}
