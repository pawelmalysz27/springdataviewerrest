package com.example.model;


import java.time.*;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="serie")
public class Seria {
	
	@Id
	private Long id;
	private String name;
	private String file;
	private UUID cassid;
	private Long categoryid;
	private LocalDate datefrom;
	private int frequency;
	private Long[] markers;
	private String unit;
	private String feed;
	private Long userid;
	private LocalDateTime createdate;
	private LocalDateTime changedate;
	
	protected Seria() {}
	
	public Seria(Long id, String name, String file, UUID cassid, Long categoryid, LocalDate datefrom, int frequency, Long[] markers, String unit, String feed, Long userid, LocalDateTime createdate, LocalDateTime changedate)
	{
		this.id = id;
		this.name = name;
		this.file = file;
		this.cassid = cassid;
		this.categoryid = categoryid;
		this.datefrom = datefrom;
		this.frequency = frequency;
		this.markers = markers;
		this.unit = unit;
		this.feed = feed;
		this.userid = userid;
		this.createdate = createdate;
		this.changedate = changedate;
	}
	
	public void setId(Long Id)
	{
		id = Id;
	}
	public Long getId()
	{
		return id;
	}
	
	public void setName(String Name)
	{
		name = Name;
	}
	public String getName()
	{
		return name;
	}
	
	public void setFile(String File)
	{
		file = File;
	}
	public String getFile()
	{
		return file;
	}
	
	public void setCassid(UUID Cassid)
	{
		cassid = Cassid;
	}
	public UUID getCassid()
	{
		return cassid;
	}
	
	public void setCategoryid(Long Categoryid)
	{
		categoryid = Categoryid;
	}
	public Long getCategoryid()
	{
		return categoryid;
	}
	
	public void setDatefrom(LocalDate Datefrom)
	{
		datefrom = Datefrom;
	}
	public LocalDate getDatefrom()
	{
		return datefrom;
	}
	
	public void setFrequency(int Frequency)
	{
		frequency = Frequency;
	}
	public int getFrequency()
	{
		return frequency;
	}
	
	public void setMarkers(Long[] Markers)
	{
		markers = Markers;
	}
	public Long[] getMarkers()
	{
		return markers;
	}
	
	public void setUnit(String Unit)
	{
		unit = Unit;
	}
	public String getUnit()
	{
		return unit;
	}
	
	public void setFeed(String Feed)
	{
		feed = Feed;
	}
	public String getFeed()
	{
		return feed;
	}
	
	public void setUserid(Long Userid)
	{
		userid = Userid;
	}
	public Long getUserid()
	{
		return userid;
	}
	
	public void setCreatedate(LocalDateTime Createdate)
	{
		createdate = Createdate;
	}
	public LocalDateTime getCreatedate()
	{
		return createdate;
	}
	
	public void setChangedate(LocalDateTime Changeedate)
	{
		changedate = Changeedate;
	}
	public LocalDateTime getChangedate()
	{
		return changedate;
	}
	
	
}