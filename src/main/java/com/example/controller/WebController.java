package com.example.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.SeriaDao;
import com.example.model.Seria;


@RestController
public class WebController extends HttpServlet{
	
	@Autowired
	SeriaDao sed;

	int draw = 0;
	int start = 0;
	int length = 5;
	
	@GetMapping("/pgTabelka")
	public Map<String,Object> pgTable(HttpServletRequest request)
	{
		Map<String,Object> json = new HashMap<String,Object>();
		if(request.getParameter("draw")!=null)
		{
			draw = Integer.parseInt(request.getParameter("draw"));
		}
		if(request.getParameter("start")!=null)
		{
			start = Integer.parseInt(request.getParameter("start"))/10;
		}
		if(request.getParameter("length")!=null)
		{
			length = Integer.parseInt(request.getParameter("length"));
		}
		int totalRecords = sed.recordsTotal();
		List<Seria> serie = sed.findPart(start, length);
		
			json.put("draw", draw);
			json.put("recordsTotal", totalRecords);
			json.put("recordsFiltered", totalRecords);
			json.put("data", serie);
		
		
		return json;
	}
}
