package com.example.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.repo.SeriaRepository;
import com.example.model.Seria;

@Service
public class SeriaDao {

	@Autowired
	SeriaRepository seriaRepo;
	
	public List<Seria> findAll(){
		List<Seria> result = new ArrayList<Seria>();
		
		for(Seria s : seriaRepo.findAll())
		{
			result.add(s);
		}
		
		return result;
	}
	
	public List<Seria> findPart(int start, int length)
	{
		Page<Seria> pg = seriaRepo.findAll(PageRequest.of(start,length));
		
		return pg.getContent();
	}
	
	public int recordsTotal()
	{
		return (int)seriaRepo.count();
	}
}
